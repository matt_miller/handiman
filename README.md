# Handiman

This is useful as a data backend for some kind of dispaly which you create.

Weights are set vi `Config`:

```
config :handiman,
  side: [
    csv: "priv/sides.csv",
    uri: "https://www.thepredictiontracker.com/nflpredictions.csv",
    weights: [
      {"talis", 10},
      {"excel", 8},
      {"pfz", 5},
      {"l2to", 5},
      {"midweek", 5},
      {"open", 2},
      {"ca", 2},
      {"dok", 2}
    ]
  ],
  tote: [
    csv: "priv/totes.csv",
    uri: "https://www.thepredictiontracker.com/nfltotals.csv",
    weights: [
      {"talis", 10},
      {"excel", 8},
      {"pfz", 5},
      {"l2to", 5},
      {"midweek", 5},
      {"open", 2},
      {"ca", 2},
      {"dok", 2}
    ]
  ]

```

Negative weights are supported as a "fade this capper" signal.
