import Config

config :logger, :console, level: :warning

config :handiman,
  side: [
    csv: "priv/sides.csv",
    uri: "https://www.thepredictiontracker.com/nflpredictions.csv",
    weights: [
      {"midweek", 3},
      {"kasulis", 3},
      {"clean", 3},
      {"donchess", 3},
      {"ffw", 3},
      {"esp2", 2},
      {"bihl", 2},
      {"log", 2},
      {"l2h", 1},
      {"open", 1},
      {"ca", 1},
      {"excel", -1},
      {"excel2", -1}
    ]
  ],
  tote: [
    csv: "priv/totes.csv",
    uri: "https://www.thepredictiontracker.com/nfltotals.csv",
    weights: [
      {"midweek", 6},
      {"open", 5},
      {"argh", 2},
      {"ffw", 2},
      {"talis", 1},
      {"ashby", 1},
      {"bihl", 1},
      {"hanson", -2}
    ]
  ]
