defmodule Handiman do
  @moduledoc """
  NFL handicapping meta-model
  """
  alias Handiman.{Spread, Total}

  @props %{:side => [:home, :road], :tote => [:over, :under]}

  @doc """
  List the available mode atoms
  """
  def modes, do: [:tote, :side]

  @doc """
  Application data key for the associated mode
  """
  def mode_key(mode)
  def mode_key(:tote), do: :tote_data
  def mode_key(:side), do: :side_data

  def matchups(which \\ :side) do
    Application.fetch_env!(:handiman, mode_key(which))
    |> Stream.map(fn m -> matchup(m, :home, which) end)
    |> Stream.reject(fn c -> c == :error end)
    |> Stream.map(fn {:ok, t, o, v} -> {t, o, v} end)
    |> Enum.to_list()
  end

  defp compute_kelly(bits, opts) do
    br = Keyword.get(opts, :bankroll, 10_000)
    count = Keyword.get(opts, :max_count, 16)
    min_pct = Keyword.get(opts, :min_pct, 1.0)

    case PainStaking.kelly(bits, independent: true, bankroll: br) do
      {:error, _} ->
        []

      {:ok, []} ->
        []

      {:ok, vals} ->
        rounded =
          vals
          |> Enum.sort(&(elem(&1, 1) <= elem(&2, 1)))
          |> Enum.map(fn {d, a} -> {d, trunc(a)} end)

        oversize = length(rounded) - count
        [{d, v} | _] = rounded

        cond do
          oversize > 0 ->
            keepers = rounded |> Enum.drop(oversize) |> Enum.map(fn {d, _} -> d end)
            compute_kelly(Enum.filter(bits, fn e -> elem(e, 0) in keepers end), opts)

          v < min_pct * br / 100 ->
            compute_kelly(Enum.reject(bits, fn e -> elem(e, 0) == d end), opts)

          true ->
            Enum.reverse(rounded)
        end
    end
  end

  @doc """
  The computed wager table, based on the application values
  Options:
    bankroll: full bankroll amount, defaults to 10,000
    max_count: maximum number of wagers to show, defaults to 16
    min_pct:  smallest wager to show relative to bankroll, defaults to 1.0
    offered: the expected offered odds, defaults to [us: "-110"]
  """
  def wager_table(opts \\ []) do
    opts |> gather_results() |> Enum.map(fn {f, s} -> {f, round(s)} end)
  end

  @doc """
  The computed spreads table, based on the application values

  Options as elsewhere
  """
  def spread_table(opts \\ []) do
    weights =
      Keyword.get(
        opts,
        :side,
        Application.fetch_env!(:handiman, :side) |> Keyword.get(:weights, [])
      )

    predict_val(weights, :side)
    |> Enum.map(fn {r, h, s, a, i} -> {r, h, displayas(s, 3), a, i} end)
  end

  @doc """
  The computed totals table, based on the application values

  Options as elsewhere
  """
  def total_table(opts \\ []) do
    weights =
      Keyword.get(
        opts,
        :tote,
        Application.fetch_env!(:handiman, :tote) |> Keyword.get(:weights, [])
      )

    predict_val(weights, :tote)
    |> Enum.map(fn {r, h, s, a, i} -> {r, h, displayas(:erlang.abs(s), 3), a, i} end)
  end

  @doc """
  The predicted outcomes as computed from the computed spreads and totals
  """
  def pred_outcome_table(opts \\ []) do
    build_outcomes(spread_table(opts), total_table(opts), [])
  end

  defp build_outcomes([], _, acc), do: Enum.reverse(acc)

  defp build_outcomes([{r, h, p, _, which} = side | rest], totals, acc) do
    {spread, _} = Float.parse(p)

    a =
      case total_for(totals, side) do
        :error ->
          acc

        total ->
          case which do
            :road ->
              rt = trunc(Float.round((total - spread) / 2))
              [{{r, rt}, {h, total - rt}} | acc]

            :home ->
              ht = trunc(Float.round((total - spread) / 2))
              [{{r, total - ht}, {h, ht}} | acc]
          end
      end

    build_outcomes(rest, totals, a)
  end

  defp total_for([], _), do: :error

  defp total_for([{tr, th, t, _, dir} | rest], {r, h, _, _, _} = side) do
    {tot, _} = Float.parse(t)

    case {tr, th} do
      {^r, ^h} ->
        case dir do
          :under -> trunc(tot)
          :over -> trunc(Float.round(tot))
        end

      _ ->
        total_for(rest, side)
    end
  end

  defp gather_results(opts) do
    opts
    |> Keyword.get(:offered, us: "-110")
    |> tables_to_edges(opts)
    |> compute_kelly(opts)
  end

  defp tables_to_edges(offered, opts) do
    (total_table(opts) ++ spread_table(opts))
    |> Enum.reduce([], fn t, a -> [edge_for(t, offered) | a] end)
  end

  defp edge_for({r, h, f, a, which}, o) when which in [:over, :under] do
    {Enum.join([r <> h, which |> Atom.to_string() |> String.upcase(), a], " "),
     [prob: 0.5 + (Total.to_prob(a, which) - Total.to_prob(f, which))], o}
  end

  defp edge_for({r, h, f, a, which}, o) when which in [:road, :home] do
    {winner, loser} =
      case which do
        :home -> {h, r}
        :road -> {r, h}
      end

    {Enum.join([winner, "COVERS", a, "(" <> loser <> ")"], " "),
     [prob: 0.5 + (Spread.to_prob(f) - Spread.to_prob(a))], o}
  end

  defp displayas(n, p) do
    n |> :erlang.float_to_list(decimals: p) |> to_string
  end

  defp match_val(m, s, w, which) do
    case matchup(m, s, which) do
      :error ->
        {}

      {:ok, r, h, act} ->
        {f, a, n} = choose_side(s, fair_spread(m, s, w, which), act)
        {r, h, f, a, n}
    end
  end

  defp choose_side(which, fair, act)

  defp choose_side(:over, fair, act) do
    {a, _} = Float.parse(act)

    case fair >= a do
      false -> {fair, a, :under}
      true -> {fair, a, :over}
    end
  end

  defp choose_side(:home, fair, act) do
    {a, _} = Float.parse(act)

    case fair <= a do
      false -> {fair * -1, a * -1, :road}
      true -> {fair, a, :home}
    end
  end

  defp matchup(m, side, which) do
    case line(m, side, for: which) do
      :otb ->
        :error

      val ->
        {:ok, team(m, :road), team(m, :home), val}
    end
  end

  defp team(m, :home), do: "@" <> Map.fetch!(m, "home")
  defp team(m, :road), do: Map.fetch!(m, "road")

  defp adjusted_spread(_m, _side, _which, [], {s, a}), do: s / a

  defp adjusted_spread(m, side, which, [{k, w} | t], {s, a}) do
    case line(m, side, alt: k, for: which) do
      :otb ->
        adjusted_spread(m, side, which, t, {s, a})

      ns ->
        {n, ""} = Float.parse(ns)

        ds =
          case w >= 0 do
            false ->
              {fs, ""} = Float.parse(line(m, side, []))
              2 * fs - n

            true ->
              n
          end

        absw = abs(w)
        adjusted_spread(m, side, which, t, {s + ds * absw, a + absw})
    end
  end

  defp vis_sort(s) do
    s
    |> List.flatten()
    |> Enum.reject(fn t -> t == {} end)
    |> Enum.sort(fn {_, h, _, _, _}, {_, o, _, _, _} -> h < o end)
  end

  defp predict_val(w, which) do
    Application.fetch_env!(:handiman, mode_key(which))
    |> Enum.reduce([], fn m, a -> [match_val(m, List.first(@props[which]), w, which) | a] end)
    |> vis_sort
  end

  defp fair_spread(m, side, weights, which) do
    case Float.parse(line(m, side, [])) do
      {:error, _} -> 0
      {actual, ""} -> adjusted_spread(m, side, which, weights, {actual, 1.0})
    end
  end

  defp line(m, side, opts) do
    f = Keyword.get(opts, :for, :side)
    a = Keyword.get(opts, :alt, "")

    key =
      case {f, a} do
        {_, ""} -> "line"
        {:side, k} -> "line" <> k
        {:tote, k} -> "tot" <> k
      end

    case {Map.get(m, key), side, f} do
      {"", _, _} -> :otb
      {nil, _, _} -> :otb
      {num, _, :tote} -> num
      {num, :over, _} -> num
      {num, :under, _} -> num
      {<<"-", _r::binary>> = neg, :road, :side} -> neg
      {<<"-", r::binary>>, :home, :side} -> "+" <> r
      {pos, :road, :side} -> "+" <> pos
      {pos, :home, :side} -> "-" <> pos
    end
  end
end
