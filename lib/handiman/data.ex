defmodule Handiman.Data do
  @moduledoc """
  Online data handling for Handiman
  """

  @doc """
  Convenience function to fetch and load the data into the application
  """
  def fetch_and_load(which \\ [:side, :tote]) do
    :httpc.set_options(
      socket_opts: [verify: :verify_peer, cacerts: :public_key.cacerts_load(CAStore.file_path())]
    )

    :ok = fetch(which)
    :ok = load(which)
  end

  @doc """
  Fetch the online handicapping data and store on the filesystem
  """
  def fetch([]), do: :ok

  def fetch([sym | rest] = syms) do
    config = Application.fetch_env!(:handiman, sym)

    case :httpc.request(:erlang.binary_to_list(config[:uri])) do
      {:ok, {_status, _headers, body}} ->
        File.write(config[:csv], body)
        fetch(rest)

      _ ->
        Process.sleep(17203)
        fetch(syms)
    end
  end

  @doc """
  Load the data from the filesystem into the application
  """
  def load([]), do: :ok

  def load([sym | rest]) do
    config = Application.fetch_env!(:handiman, sym)

    Application.put_env(:handiman, Handiman.mode_key(sym), csv_map(config[:csv]),
      persistent: true
    )

    load(rest)
  end

  def csv_map(file) do
    file
    |> File.stream!()
    |> CSV.decode(headers: true)
    |> Stream.map(fn {:ok, v} -> v end)
    |> Enum.to_list()
  end
end
