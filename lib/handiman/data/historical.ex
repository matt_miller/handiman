defmodule Handiman.Data.Historical do
  alias Handiman.{Data, Util}

  @moduledoc """
  Import and expose historical results data
  """

  @seasons 17
  @historical_csv Path.expand("priv/spreadspoke_scores.csv")
  @full_data Data.csv_map(@historical_csv)
  # Maybe it won't stay in proper order as we get new data 
  # Including playoff data without distinction for now
  @proper_data Enum.reduce(@full_data, {0, []}, fn entry, {latest_season, acc} ->
                 sn = String.to_integer(Map.get(entry, "schedule_season"))
                 hn = String.to_integer(Map.get(entry, "score_home"))
                 an = String.to_integer(Map.get(entry, "score_away"))

                 {Enum.max([latest_season, sn]),
                  [
                    %{total: an + hn, spread: abs(hn - an), season: sn} | acc
                  ]}
               end)
               |> then(fn {latest_season, acc} ->
                 Enum.filter(acc, fn m -> m.season > latest_season - @seasons end)
               end)
  # A lot of this is inefficient.  I don't care because
  # - it's smallish
  # - it's only done at compile time
  @totals Util.hook_under_map(@proper_data, :total)
  @spreads Util.hook_under_map(@proper_data, :spread)

  high = Map.get(@totals, :max)
  low = Map.get(@totals, :min)
  size = Map.get(@totals, :count)

  def under(n) when is_integer(n), do: under(n * 1.0)
  def under(n) when n < unquote(low), do: 0.0
  def under(n) when n > unquote(high), do: 1.0

  for key <- @totals |> Map.keys() |> Enum.filter(&is_number/1) do
    def under(unquote(key)), do: unquote(Map.get(@totals, key) / size)
  end

  def under(x), do: interpolate(x, &under/1)

  def over(n) when is_integer(n), do: over(n * 1.0)
  def over(n) when n < unquote(low), do: 1.0
  def over(n), do: 1.0 - under(n)

  top = Map.get(@spreads, :max)
  all = Map.get(@spreads, :count)

  def spread(n) when is_integer(n), do: spread(n * 1.0)
  def spread(0.0), do: 0.50
  def spread(n) when n >= unquote(top), do: 0.0
  def spread(n) when n <= unquote(top * -1), do: 1.0

  for key <- @spreads |> Map.keys() |> Enum.filter(fn n -> is_number(n) and n > 0 end) do
    def spread(unquote(key)), do: 0.5 - unquote(Map.get(@spreads, key) / (2 * all))
    def spread(unquote(key * -1)), do: 1.0 - spread(unquote(key))
  end

  def spread(x), do: interpolate(x, &spread/1)

  def interpolate(x, fun) do
    # Simple linear for now
    x_0 = Util.nearest_half(x)

    x_1 =
      case x_0 > x do
        true -> x_0 - 0.5
        false -> x_0 + 0.5
      end

    (fun.(x_0) * (x_1 - x) + fun.(x_1) * (x - x_0)) / (x_1 - x_0)
  end
end
