defmodule Handiman.Spread do
  @moduledoc """
  NFL spread meta-model
  """
  alias Handiman.Data.Historical

  @doc """
  Convert a probability into the implied spread
  """
  def from_prob(prob)
  def from_prob(prob), do: from_prob(-59.5, 59.5, prob)

  def from_prob(l, h, prob) do
    mid = (l + h) / 2
    diff = prob - Historical.spread(mid)

    cond do
      abs(diff) < 0.0005 or l >= h ->
        mid

      diff > 0 ->
        from_prob(l, mid, prob)

      true ->
        from_prob(mid, h, prob)
    end
  end

  @doc """
  Convert a spread into an implied probability
  """
  def to_prob(n)

  def to_prob(n) when is_binary(n) do
    {rn, _} = Float.parse(n)
    to_prob(rn)
  end

  def to_prob(n) when is_integer(n), do: to_prob(n * 1.0)

  def to_prob(n), do: Historical.spread(n)
end
