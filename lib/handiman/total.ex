defmodule Handiman.Total do
  @moduledoc """
  NFL totals meta-model
  """

  alias Handiman.Data.Historical

  @doc """
  Convert an implied probability to a total
  """

  def from_prob(n, side)
  def from_prob(prob, side), do: from_prob(0, 200, prob, side)

  def from_prob(l, h, prob, side) do
    mid = (l + h) / 2

    implied =
      case side do
        :under -> Historical.under(mid)
        :over -> Historical.over(mid)
      end

    diff = prob - implied

    cond do
      abs(diff) < 0.0005 or l >= h ->
        mid

      (diff > 0 and side == :over) or (diff < 0 and side == :under) ->
        from_prob(l, mid, prob, side)

      true ->
        from_prob(mid, h, prob, side)
    end
  end

  @doc """
  Convert a total to the implied probability
   
  Requires :over or :under for computation
  """
  def to_prob(n, side)

  def to_prob(n, side) when is_binary(n) do
    {rn, _} = Float.parse(n)
    to_prob(rn, side)
  end

  def to_prob(n, side) do
    case side do
      :over -> Historical.over(n)
      :under -> Historical.under(n)
    end
  end
end
