defmodule Handiman.Util do
  @moduledoc """
  Utility functions for Handiman
  """

  @doc """
  Determine if two numbers have the same sign
  """
  def samesign?(x, y) when x < 0 and y < 0, do: true
  def samesign?(x, y) when x >= 0 and y >= 0, do: true
  def samesign?(_x, _y), do: false

  @doc """
  Round to the nearest half point
  """
  def nearest_half(x), do: Float.round(x / 0.5) * 0.5

  @doc """
  Count the number of entries under each half point ties are a 0.5 win
  """
  def hook_under_map(list, key), do: per_entry(list, key, %{})

  defp per_entry([], _key, acc),
    do: acc |> key_tuple |> desparse |> sum_with_hooks()

  defp per_entry([entry | rest], key, acc) do
    na =
      acc
      |> Map.update(Map.get(entry, key), 1.0, fn e -> e + 1 end)

    per_entry(rest, key, na)
  end

  defp key_tuple(map), do: {Map.keys(map) |> Enum.sort(), map}

  defp desparse({[first | rest], map}), do: fill_holes({rest, map}, first)
  defp fill_holes({[], map}, _), do: key_tuple(map)

  defp fill_holes({[entry | rest], map}, prev) when entry == prev + 1 do
    fill_holes({rest, map}, entry)
  end

  defp fill_holes({[entry | rest], map}, prev) do
    val = Map.get(map, prev)

    fill_holes(
      {rest, Enum.reduce((prev + 1)..entry, map, fn e, a -> Map.put(a, e, val) end)},
      entry
    )
  end

  defp sum_with_hooks({skeys, map}) do
    sum_with_hooks({skeys, map}, 0, %{min: List.first(skeys), max: List.last(skeys)})
  end

  defp sum_with_hooks({[], _map}, count, acc), do: Map.merge(acc, %{count: count})

  defp sum_with_hooks({[key | rest], map}, count, acc) do
    howmany = Map.get(map, key)
    ucount = count + howmany
    pushval = count + howmany * 0.5

    sum_with_hooks(
      {rest, map},
      ucount,
      Map.merge(acc, %{(key * 1.0) => pushval, (key + 0.5) => ucount})
    )
  end
end
