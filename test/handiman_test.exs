defmodule HandimanTest do
  use ExUnit.Case
  doctest Handiman

  test "prob to spread" do
    oof = Enum.map(500..1000, fn x -> Handiman.Spread.from_prob(x / 1000) end)
    assert oof == Enum.sort(oof, &(&1 >= &2)), "Monotonically decreasing to +inf"
    ouch = Enum.map(500..0, fn x -> Handiman.Spread.from_prob(x / 1000) end)
    assert ouch == Enum.sort(ouch), "Monotonically increasing to -inf"
  end

  test "spread to prob" do
    oof = Enum.map(-1613..1613, fn x -> Handiman.Spread.to_prob(x / 30) |> Float.round(5) end)

    assert oof == Enum.sort(oof, &(&1 >= &2)),
           "Monotonically decreasing to 0 in a reasonable range"
  end
end
